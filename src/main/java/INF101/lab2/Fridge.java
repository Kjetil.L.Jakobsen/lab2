package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> fridge = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return fridge.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            fridge.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)) {
            fridge.remove(item);
        } else {
            throw new NoSuchElementException();
        } 
    }

    @Override
    public void emptyFridge() {
        fridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem i : fridge) {
            if (i.hasExpired()) {
                expiredFood.add(i);
            }
        }
        for (FridgeItem j : expiredFood) {
            fridge.remove(j);
            }
        return expiredFood;
    }    
}
